import React from 'react'
import { navigate, Link } from 'gatsby'
import styled from '@emotion/styled'

const Wrapper = styled.div`
  width: 100%;
  margin: -1.5rem auto 2.5rem;
  max-width: ${props => props.theme.sizes.maxWidth};
  padding: 0 1.5rem;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: baseline;
`

const RightButtonContainer = styled.div`
  float: right;
`

const Button = styled(Link)`
  background: ${props => props.theme.colors.background};
  color: ${props => props.theme.colors.text};
  padding: 1rem;
  border-radius: 20px;
  border-style: solid;
  border-width: 1px;
  border-color: ${props => props.theme.colors.primary};
  margin: 0 0 0 0.5rem;
  cursor: pointer;
  text-decoration: none;
  transition: 0.3s all;
  &:hover {
    background: ${props => props.theme.colors.secondary};
  }
  @media (hover: none) {
    background: ${props => props.theme.colors.primary} !important;
  }
`

const Pagination = props => {
  function changePage(e) {
    navigate(
      e.target.value
        ? `${props.context.paginationPath}/${e.target.value}`
        : `${props.context.paginationPath}/`
    )
  }

  return (
    <>
      {props.context.numberOfPages > 1 && (
        <Wrapper>
          <div>
            {props.context.previousPagePath && (
              <Button to={`${props.context.previousPagePath}`}>
                <span>&larr;</span> Prev
              </Button>
            )}
          </div>
          <RightButtonContainer>
            {props.context.nextPagePath && (
              <Button style={{ order: 3 }} to={`${props.context.nextPagePath}`}>
                Next <span>&rarr;</span>
              </Button>
            )}
          </RightButtonContainer>
        </Wrapper>
      )}
    </>
  )
}

export default Pagination
