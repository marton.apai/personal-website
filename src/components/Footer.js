import React from 'react'
import styled from '@emotion/styled'

const Wrapper = styled.footer`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  margin: 0 auto;
  max-width: ${props => props.theme.sizes.maxWidth};
`

const List = styled.ul`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  border-top: 1px solid ${props => props.theme.colors.secondary};
  padding: 1em 0 2em;
  margin: 0 1.5em;
`

const InnerList = styled.ul`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  padding: 0 0 2em;
  margin: 0 1.5em;
`

const Item = styled.li`
  display: inline-block;
  padding: 0.25em 0;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    width: auto;
  }
  a {
    font-size: 75%;
    font-weight: 400;
    transition: all 0.2s;
    color: ${props => props.theme.colors.text};
    &:hover {
      color: ${props => props.theme.colors.highlight};
    }
    &:visited {
      color: ${props => props.theme.colors.text};
    }
  }
`

const Footer = () => (
  <Wrapper>
    <List>
      <Item>
        <a
          href="https://www.linkedin.com/in/marton-apai/"
          rel="nofollow noopener noreferrer"
          target="_blank"
        >
          <img
            src="/images/In-2C-54px-R.png"
            style={{ width: '30px' }}
            alt="LinkedIn"
          />
        </a>
      </Item>
      <Item>
        <a
          href="https://www.facebook.com/marton.apai/"
          rel="nofollow noopener noreferrer"
          target="_blank"
        >
          <img
            src="/images/f-ogo_RGB_HEX-58.png"
            style={{ width: '25px' }}
            alt="Facebook"
          />
        </a>
      </Item>
      <Item>
        <a
          href="https://www.instagram.com/marton.apai/"
          rel="nofollow noopener noreferrer"
          target="_blank"
        >
          <img
            src="/images/glyph-logo_May2016-310x310.png"
            style={{ width: '30px' }}
            alt="Instagram"
          />
        </a>
      </Item>
      <Item>
        <a
          href="https://twitter.com/MartonApai"
          rel="nofollow noopener noreferrer"
          target="_blank"
        >
          <img
            src="/images/Twitter_Logo_Blue-310x310.png"
            style={{ width: '30px' }}
            alt="Twitter"
          />
        </a>
      </Item>
      <Item>
        <a
          href="https://gitlab.com/marton.apai"
          rel="nofollow noopener noreferrer"
          target="_blank"
        >
          <img
            src="/images/gitlab-logo-gray-stacked-rgb.png"
            style={{ width: '45px' }}
            alt="Twitter"
          />
        </a>
      </Item>
      <Item>
      </Item>
      <Item>
      </Item>
      <Item>
        <InnerList>
          <Item>
            <a
              href="https://www.gatsbyjs.org/"
              rel="nofollow noopener noreferrer"
              target="_blank"
            >
              <img
                src="/images/Gatsby_Logo.png"
                style={{ width: '100px' }}
                alt="Powered by GatsbyJS"
              />
            </a>
          </Item>
          <Item>
            <a
              href="https://www.contentful.com/"
              rel="nofollow noopener noreferrer"
              target="_blank"
            >
              <img
                src="https://images.ctfassets.net/fo9twyrwpveg/44baP9Gtm8qE2Umm8CQwQk/c43325463d1cb5db2ef97fca0788ea55/PoweredByContentful_LightBackground.svg"
                style={{ width: '100px' }}
                alt="Powered by Contentful"
              />
            </a>
          </Item>
          <Item>
            <a
              href="https://github.com/ryanwiemer/gatsby-starter-gcn"
              target="_blank"
              rel="noopener noreferrer"
            >
              gatsby-starter-gcn
            </a>{' '}
            <br/>
            <a
              href="https://github.com/ryanwiemer"
              target="_blank"
              rel="noopener noreferrer"
            >
              @ryanwiemer
            </a>
          </Item>
        </InnerList>
      </Item>
    </List>
  </Wrapper>
)

export default Footer
